package main

import (
	"errors"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"

	"bitbucket.org/anphung/queue/api"
	"bitbucket.org/anphung/queue/handler"
	"bitbucket.org/anphung/queue/utils"
	"bitbucket.org/anphung/queue/worker"
)

func main() {
	// Create queue api server
	port := utils.Getenv("QUEUE_PORT", "8080")
	server := api.NewQueue()

	// Connect to mysql database
	dsn := utils.Getenv("QUEUE_MYSQL_DSN", "")
	if dsn == "" {
		panic(errors.New("QUEUE_MYSQL_DSN is empty"))
	}
	db, err := gorm.Open("mysql", dsn)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	// Start workers
	go worker.Start(db, 10, 1)

	server.Echo.POST("api/v1/tasks", handler.HTTPSQLTask(db))

	server.Run(port)
}
