// Package handler contains echo handler for handling task request.
package handler

import (
	"database/sql"
	"database/sql/driver"
	"encoding/json"
	"net/http"
	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/labstack/echo"
)

// Type TaskData is type for storing json object.
type TaskData string

func (t *TaskData) MarshalJSON() ([]byte, error) {
	return []byte(*t), nil
}

func (t *TaskData) UnmarshalJSON(data []byte) error {
	*t = TaskData(data)
	return nil
}

func (t TaskData) Value() (driver.Value, error) {
	return string(t), nil
}

func (t *TaskData) Scan(src interface{}) error {
	s, ok := src.([]byte)
	if !ok {
		return nil
	}
	*t = TaskData(s)
	return nil
}

// TaskData implements the below interfaces to satisfy both
// json parser and sql parser
var _ json.Marshaler = (*TaskData)(nil)
var _ json.Unmarshaler = (*TaskData)(nil)
var _ sql.Scanner = (*TaskData)(nil)
var _ driver.Valuer = (*TaskData)(nil)

// Task struct stores task.
type Task struct {
	gorm.Model
	URL              string    `json:"url"`                                             // where the job should be sent
	Method           string    `json:"method"`                                          // POST/GET/PUT/DELETE
	Data             TaskData  `json:"data" sql:"type:json DEFAULT NULL"`               // data needed for the task request
	RetryUntil       time.Time `json:"retry_until"`                                     // when it should stop retrying
	CreatedByRequest TaskData  `json:"created_by_request" sql:"type:json DEFAULT NULL"` // information about the request that created the queue item for debugging purposes
}

// HTTPSQLTask wraps around handler that recieves and store http task request
// to sql database
func HTTPSQLTask(db *gorm.DB) func(c echo.Context) error {
	if !db.HasTable(&Task{}) {
		db.CreateTable(&Task{})
	}
	db.AutoMigrate(&Task{})

	return func(c echo.Context) error {
		// Parse payload
		task := new(Task)
		err := c.Bind(task)
		if err != nil {
			log.Error(err)
			return c.JSON(http.StatusBadRequest, nil)
		}

		db.Create(task)
		return c.JSON(http.StatusOK, nil)
	}
}
