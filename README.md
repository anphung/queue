# queue #

A http task queue.

### Usage ###
```
go get -u bitbucket.org/anphung/queue

cd queue
QUEUE_MYSQL_DSN="root:my-secret-pw@(localhost:3306)/my_database?parseTime=true" go run queue.go
```