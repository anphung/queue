// Package queue implement RESTful api that receives and stores task.
package api

import (
	"fmt"

	log "github.com/Sirupsen/logrus"
	"github.com/labstack/echo"
)

// Queue is an API server that receives and stores task
type Queue struct {
	*echo.Echo // API server
}

// NewQueue initializes and returns new queue server
func NewQueue() *Queue {
	// Our server
	e := echo.New()

	s := &Queue{
		e,
	}

	return s
}

// Bind binds route to hander
func (s *Queue) Bind(r string, h func(echo.Context) error) {
	s.Echo.POST(r, h)
}

// Run runs Queue server
func (s *Queue) Run(port string) {
	log.Printf("Listening on port %s", port)
	s.Logger.Fatal(s.Start(fmt.Sprintf(":%s", port)))
}
