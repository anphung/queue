package utils

import (
	"os"
)

// Getenv returns values of environment variable, if not found returns def
func Getenv(name, def string) string {
	v := os.Getenv(name)
	if v == "" {
		v = def
	}

	return v
}
