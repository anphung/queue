// Driver function to illustrate how to use worker
package main

import (
	"errors"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"

	"bitbucket.org/anphung/queue/utils"
	"bitbucket.org/anphung/queue/worker"
)

func main() {
	// Connect to mysql database
	dsn := utils.Getenv("QUEUE_MYSQL_DSN", "")
	if dsn == "" {
		panic(errors.New("QUEUE_MYSQL_DSN is empty"))
	}
	db, err := gorm.Open("mysql", dsn)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	worker.Start(db, 10, 1)
}
