package worker

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"net/http"
	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"

	"bitbucket.org/anphung/queue/handler"
)

type CompletedTask struct {
	handler.Task
}

const DefaultNumberOfWorkers = 100

// Start creates and starts numberOfWorkers for task.
// After every interval, we fetch all the unfinished tasks, and call
// worker to process each task.
func Start(db *gorm.DB, nWorkers int, interval int) {
	log.Info("Worker started")
	if nWorkers == 0 {
		nWorkers = DefaultNumberOfWorkers
	}

	// Create table if not exist
	if !db.HasTable(&CompletedTask{}) {
		db.CreateTable(&CompletedTask{})
	}
	db.AutoMigrate(&CompletedTask{})

	var jobs = make(chan handler.Task, nWorkers)
	// Initialize workers
	for i := 0; i < nWorkers; i++ {
		go worker(db, jobs)
	}
	// Add work to jobs channel
	var tasks []handler.Task
	for {
		// Get unfinished tasks
		db.Find(&tasks)
		for _, task := range tasks {
			jobs <- task
		}
		time.Sleep(time.Duration(interval) * time.Second)
	}
}

// DoHTTPRequest processes task.
func DoHTTPRequest(task *handler.Task) (int, error) {
	// Build http client
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}
	// Build request
	var err error
	var req *http.Request
	if task.Data != "{}" {
		payload, err := json.Marshal(task.Data)
		if err != nil {
			log.Error(err)
			return 0, err
		}
		req, err = http.NewRequest(task.Method,
			task.URL,
			bytes.NewReader(payload))
	} else {
		req, err = http.NewRequest(task.Method,
			task.URL,
			nil)
	}
	if err != nil {
		log.Error(err)
		return 0, err
	}

	resp, err := client.Do(req)
	if err != nil {
		log.Error(err)
		return 0, err
	}

	return resp.StatusCode, nil
}

func worker(db *gorm.DB, jobs <-chan handler.Task) {
	// Keep looking for task
	for task := range jobs {
		// Check if this task is expired
		if time.Now().After(task.RetryUntil) { // Expire
			db.Unscoped().Delete(&task)
			continue
		} else {
			statusCode, err := DoHTTPRequest(&task)
			log.Info(statusCode)
			if err != nil {
				log.Error(err)
				continue
			}

			// The task is successfully processed
			if (200 <= statusCode) && (statusCode < 300) {
				db.Unscoped().Delete(&task)
				cTask := &CompletedTask{
					task,
				}
				db.Create(&cTask)
				continue
			}
		}
	}
}
